package to;

import java.util.ArrayList;
import java.util.List;

// Группа технологий со своим кодом
public class RootModelTo {
    public String rootCode;
    public List<TechModelTo> techList;

    public RootModelTo(String rootCode) {
        this.rootCode = rootCode;
        this.techList = new ArrayList<>();
    }

    @Override
    public String toString() {
        return "RootModelTo{" +
                "rootCode='" + rootCode + '\'' +
                ", techList=" + techList +
                '}';
    }
}
