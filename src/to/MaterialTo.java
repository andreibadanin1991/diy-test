package to;

// Материал
public class MaterialTo {
    public String materialCode;

    public MaterialTo(String materialCode) {
        this.materialCode = materialCode;
    }

    @Override
    public String toString() {
        return "MaterialTo{" +
                "materialCode='" + materialCode + '\'' +
                '}';
    }
}
