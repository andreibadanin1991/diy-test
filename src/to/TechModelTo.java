package to;

import java.util.ArrayList;
import java.util.List;

//  Технология - набор материалов
public class TechModelTo {
    public String techCode;
    public List<MaterialTo> rowTos;

    public TechModelTo(String techCode) {
        this.techCode = techCode;
        this.rowTos = new ArrayList<>();
    }

    @Override
    public String toString() {
        return "TechModelTo{" +
                "techCode='" + techCode + '\'' +
                ", rowTos=" + rowTos +
                '}';
    }
}
