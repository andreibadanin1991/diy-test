import to.PositionType;

public class RowModel {
    public String anyCode;
    public PositionType positionType;

    public RowModel(String anyCode, PositionType positionType) {
        this.anyCode = anyCode;
        this.positionType = positionType;
    }
}
