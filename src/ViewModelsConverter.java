import to.MaterialTo;
import to.RootModelTo;
import to.TechModelTo;

import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.isNull;

public class ViewModelsConverter {

    // Сюда приходит список:  TECHNOLOGY,MATERIAL,MATERIAL,..,MATERIAL
    public static TechModelTo toTechModel(List<RowModel> source) {
        TechModelTo result = null;
        for (RowModel rowModel : source) {
            switch (rowModel.positionType) {
                case ROOT -> throw new IllegalArgumentException();
                case TECHNOLOGY -> {
                    if (result != null) throw new IllegalArgumentException();
                    result = new TechModelTo(rowModel.anyCode);
                }
                case MATERIAL -> {
                    if (result == null) throw new IllegalArgumentException();
                    result.rowTos.add(new MaterialTo(rowModel.anyCode));
                }
            }
        }
        return result;
    }

    // Сюда приходит список:  ROOT,TECHNOLOGY,MATERIAL,MATERIAL,..,MATERIAL, [ROOT,TECHNOLOGY,MATERIAL,MATERIAL,..,MATERIAL], ...
    public static List<RootModelTo> toRootModels(List<RowModel> source) {
        List<RootModelTo> roots = new ArrayList<>();
        if (source.isEmpty()) {
            return roots;
        }

        List<RowModel> materialsOfTechnology = new ArrayList<>();
        RootModelTo rootModelTo = null;
        for (RowModel rowModel : source) {
            switch (rowModel.positionType) {
                case ROOT -> {
                    flushRoot(roots, rootModelTo, materialsOfTechnology);
                    rootModelTo = new RootModelTo(rowModel.anyCode);
                }
                case TECHNOLOGY -> {
                    if (isNull(rootModelTo)) throw new IllegalArgumentException();
                    flushTechnology(rootModelTo, materialsOfTechnology);
                    materialsOfTechnology.add(rowModel);
                }
                case MATERIAL -> {
                    if (wasNoTechnologyOn(materialsOfTechnology))
                        throw new IllegalArgumentException();
                    materialsOfTechnology.add(rowModel);
                }
            }
        }
        flushRoot(roots, rootModelTo, materialsOfTechnology);
        return roots;
    }

    private static void flushRoot(List<RootModelTo> roots, RootModelTo rootModelTo, List<RowModel> materialsOfTechnology) {
        if (isNull(rootModelTo))
            return;
        flushTechnology(rootModelTo, materialsOfTechnology);
        roots.add(rootModelTo);
    }

    private static void flushTechnology(RootModelTo rootModelTo, List<RowModel> materialsOfTechnology) {
        if (materialsOfTechnology.isEmpty())
            return;
        rootModelTo.techList.add(toTechModel(materialsOfTechnology));
        materialsOfTechnology.clear();
    }

    private static boolean wasNoTechnologyOn(List<RowModel> materialsOfTechnology) {
        return materialsOfTechnology.isEmpty();
    }
}
